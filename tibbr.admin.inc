<?php

/**
 * @file
 *   Holds admin specific functions for Tibbr.
 */

/**
 * Form constructor for the Tibbr admin form.
 */
function tibbr_admin_settings_form($form, &$form_state) {
  $form['tibbr_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Tibbr host'),
    '#default_value' => variable_get('tibbr_host', ''),
    '#size' => 60,
    '#description' => t('The fully qualified hostname of the Tibbr account.'),
    '#required' => TRUE,
  );
  $form['tibbr_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Tibbr username'),
    '#default_value' => variable_get('tibbr_user', ''),
    '#size' => 60,
    '#description' => t('The username of the account that will be used to get authentication.'),
    '#required' => TRUE,
  );
  // We should reconsider this because this password is going to be stored here in the clear.
  // Is there a better solution?
  $form['tibbr_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Tibbr password'),
    '#default_value' => variable_get('tibbr_pass', ''),
    '#size' => 60,
    '#description' => t('The password associated with the account.'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
