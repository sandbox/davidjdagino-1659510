<?php

/**
 * @file
 *   @todo.
 */

/**
 * Wraps drupal_http_request() for interaction with the Tibbr API.
 *
 * @param string $endpoint
 *   The name of the endpoint to request.
 * @param string $method
 *   The HTTP method to use, defaults to 'GET'.
 * @param array $data
 *   An array of data to be parsed into XML as request input. Defaults to an
 *   empty array.
 * @param array $options
 *   An array of key-value pairs to be used as query parameters. Defaults to an
 *   empty array.
 *
 * @return object
 *   A response object as returned by drupal_http_request().
 */
function tibbr_request($endpoint, $method = 'GET', $data = array(), $options = array()) {
  $url = 'https://' . variable_get('tibbr_host', '') . '/a/' . $endpoint . '?' . drupal_http_build_query($options);
  $headers = array(
    'Content-Type' => 'text/xml',
    'client_key' => 'drupal.org',
    'auth_token' => tibbr_get_auth(),
  );
  return drupal_http_request($url, array(
    'headers' => $headers,
    'method' => $method,
    'data' => tibbr_build_xml($data),
  ));
}

/**
 * Helper function to convert an array of key-value pairs into XML
 */
function tibbr_build_xml($data) {
  $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  $xml .= '<params>' . "\n";
  foreach($data as $key => $val){
      if ($key == 'remember-me') {
        $xml .= '<' . $key . ' type="boolean"' . '>' . $val . '</' . $key . '>';
      }
      else {
        $xml .= '<' . $key . '>' . $val . '</' . $key . '>';
      }
  }
  $xml .= '</params>';
  return $xml;
}

/**
 * Retrieves an authentication token from Tibbr to use with future requests.
 */
function tibbr_get_auth() {
  $url = 'https://' . variable_get('tibbr_host', '') . '/a/users/login.xml';
  $data = array(
    'login' => variable_get('tibbr_user', ''),
    'password' => variable_get('tibbr_pass', ''),
    'remember-me' => 'false',
  );
  $headers = array(
    'Content-Type' => 'text/xml',
    'client_key' => 'drupal.org',
  );
  $response = drupal_http_request($url, array(
    'headers' => $headers,
    'method' => 'POST',
    'data' => tibbr_build_xml($data),
  ));

  // Serialize XML data into PHP array
  $a = json_decode(json_encode((array) simplexml_load_string($response->data)), 1);
  return $a['auth-token'];
}
